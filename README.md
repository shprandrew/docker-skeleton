## Docker Skeleton

#### Images
- php:7.4.0-fpm;
- nginx:1.17.8-apline;
- mysql:8.0.19;
- swaggerapi/swagger-ui.

#### Requirements
- docker;
- docker-compose;
- git.

#### Usage
1. Clone this repo to your local machine;
2. Make a copy of `.env.example` file and name it `.env`;
3. Configure all environment variables in `.env` file;
4. Run `docker-compose up -d`.

#### Environment variables:
| Variable | Description | Example | Additional |
| -------- | ----------- | ------- | ---------- |
| ABSOLUTE_PROJECT_PATH | Absolute path to the necessary project on your local machine. | /usr/local/var/www/health-check-api | - |
| PROJECT_NAME | Project name for your docker machine. Used for containers and logs names. | health_check_api | - |
| PROJECT_FOLDER | Name for your project folder inside docker container. | health-check-api | Will create `/var/www/health-check-api` folder with project files inside. |
| PROJECT_ROOT | Relative path to the `index.php` file inside your project to configure Nginx server. Should contain `PROJECT_FOLDER` value. | health-check-api/public | Example for `symfony 4` and `symfony 5`. Will set `root` value for nginx server configuration. Summary root path will be `/var/www/health-check-api/public`. |
| NGINX_HOST | Host for your project. | api.health-check.loc | Will set `server_name` for Nginx server configuration. |
| NGINX_PORT | Nginx port. | 80 | - |
| MYSQL_ROOT_PASSWORD | MySQL root password. | root | - |
| MYSQL_USER | MySQL user name. | dev | Use this variable value for your project. |
| MYSQL_PASSWORD | MySQL user password. | dev | Use this variable value for your project. |
| MYSQL_DATABASE | MySQL database name. | health_check_dev | Use this variable for your project. |
| MYSQL_PORT | Port for mysql. | 3306 | - |
| SWAGGER_PORT | Port to access swagger OpenAPI. | 7000 | To access it go to [http://localhost:7000](http://localhost:7000) page. |
| SWAGGER_NAME | Name to access OpenAPI documentation (without extension). | health_check | Go to [http://localhost:7000](http://localhost:7000) and type `health_check.yaml` to access documentation. Depends on **SWAGGER_EXTENSION**. |
| SWAGGER_EXTENSION | Swagger extension (yaml, json, etc.). | yaml | Will try to find and parse `health_check.yaml` (or `health_check.json`). Depends on **SWAGGER_NAME**. |
| PATH_TO_SWAGGER | Relative path to swagger file (without extension) inside your project | src/Resources/swagger/health_check | - |
| XDEBUG_REMOTE_HOST | Your local IP address. | 192.168.1.1 | - |
| XDEBUG_REMOTE_PORT | Remote port for xdebug connection. | 9000 | - |

#### How to apply changes for swagger OpenAPI
1. Run `./scripts/update.sh` to update swagger file inside Docker container;
2. Refresh the page.
