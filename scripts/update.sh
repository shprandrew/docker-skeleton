#!/usr/bin/env bash

ENV_FILE=`pwd`/.env
NAME=`grep PROJECT_NAME $ENV_FILE | cut -d '=' -f2`

docker exec -it "$NAME"_swagger /usr/local/bin/cp_swagger.sh;
docker exec -it "$NAME"_swagger /usr/local/bin/set_server.sh;
